# encoding: utf-8

describe Rbitter do
  it 'has version string' do
    expect(Rbitter::VERSION).to be_a(String)
  end
end
